from my_utils import *;
import sys
from watchdog.observers import Observer

dir = sys.argv[1] if len(sys.argv) > 1 else "./images"
log_lever =os.getenv("LOG_LEVEL","INFO")

logging.basicConfig(level=log_lever)


logging.info(f"checking for not downloaded files in {dir}")
#check if the local file is in S3 bucket 
# if not, download the file from S3 bucket and upload it to the local directory
s3_objects = s3.list_objects(Bucket=bucket_name).get('Contents')
if s3_objects:
    for filename in s3.list_objects(Bucket=bucket_name)['Contents']:
        image_path=os.path.join(dir,filename['Key'])
        if not os.path.exists(image_path):
            logging.info(f"Downloading {filename['Key']}")
            s3.download_file(bucket_name,filename['Key'],image_path)
else:
    logging.info("No files in bucket : {bucket_name}")

logging.info(f"checking for not uploaded files in {dir}")
# check if the file has already been uploaded to the S3 bucket
# upload the file if it hasn't been uploaded
for filename in os.listdir(dir):
    # check if the file has already been uploaded to the S3 bucket
    try:
        s3.head_object(Bucket=bucket_name, Key=filename)
    except:
        # if the file hasn't been uploaded, handle the exception
        image_path=os.path.join(dir,filename)
        text=image_to_text(image_path)
        logging.debug(f"Text in {image_path}: {text}")
        upload_image(image_path ,text,filename)




logging.info(f"monitoring for new files in {dir}")
event_handler = NewFileHandler()
observer = Observer()
observer.schedule(event_handler, dir, recursive=False)
observer.start()
observer.join()


