from shutil import copyfile
import http.server
import logging
import boto3
from urllib.parse import unquote_plus,unquote,parse_qs,urlparse
from json import dumps
from os import environ

logging.basicConfig(level=logging.INFO)

port = int(environ.get("PORT") or 3000)
bucket_name = environ.get("BUCKET_NAME")
file_server_url=environ.get("FILE_SERVER_ADDRESS")
images_dir=environ.get("IMAGES_DIR")
fs_temp_folder=environ.get("FS_TEMP_FOLDER")

assert file_server_url is not None, 'file_server_url is None'
file_server_url+="/"

s3 = boto3.client('s3')

class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        parsed_url = urlparse(self.path)
        query_params = parse_qs(parsed_url.query)
        query= query_params.get("query", [])[0]
        query=unquote(query)
        logging.info(f"query: {query}")

        bucket_response = s3.list_objects_v2(Bucket=bucket_name)
        
        files=[]

        # Check each object's metadata for the specified query
        for obj in bucket_response['Contents']:
            obj_metadata = s3.head_object(Bucket=bucket_name, 
                                          Key=obj['Key'])['Metadata']
            file_text=unquote_plus(obj_metadata['text'])
            logging.debug(f"Text in {obj['Key']}: {file_text}")
            if query in file_text:
                image_name=obj['Key']
                logging.info(f"Object {image_name} contains text with query '{query}'")
                files.append(
                        {'name':image_name,
                         'url':file_server_url+obj['Key'],
                         'text': file_text
                         })
                copyfile(images_dir+image_name ,fs_temp_folder+image_name)
                 

        
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        message = dumps({'files':files})
        self.wfile.write(message.encode())
        


httpd = http.server.HTTPServer(("", port), RequestHandler)
logging.info(f"serving at port: {port}")
httpd.serve_forever()

