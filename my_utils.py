from boto3.exceptions import S3UploadFailedError
import pytesseract
import cv2
import os
import boto3
from urllib.parse import quote_plus
from watchdog.events import FileSystemEventHandler
import logging

#logging.basicConfig(level=logging.DEBUG)
bucket_name = os.environ.get("BUCKET_NAME")

s3 = boto3.client('s3')

class NewFileHandler(FileSystemEventHandler):
    def on_created(self, event):
        logging.info(f'New file created: {event.src_path}')
        image_path = event.src_path
        text=image_to_text(image_path)
        if text is None:
            logging.info(f"Not readable image. Deleting {image_path}")
            os.remove(image_path)
            return

        logging.debug(f"NewFileHandler: Text in {image_path}: {text}")
        upload_image(image_path,text,os.path.basename(image_path))

def image_to_text(image_path,lang="eng+ukr"):
    image = cv2.imread(image_path, cv2.IMREAD_COLOR)
    if image is None:
        return None
    config = f"-l {lang}"
    text_list = pytesseract.image_to_string(image,config=config)
    text = " ".join(text_list.split())
    return text

def upload_image(image_path,text,image_name):
        metadata={'text': quote_plus(text)}
        logging.debug(f"Text in {image_path}: {text}")
        logging.debug(f"Image name: {image_name}")
        logging.debug(f"Image path: {image_path}")
        try:
            s3.upload_file(image_path,bucket_name,image_name,
                       ExtraArgs={'Metadata':metadata})
        except S3UploadFailedError as e:
            logging.critical(e)
        logging.info(f"Uploaded {image_name}")

