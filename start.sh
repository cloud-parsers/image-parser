#/bin/sh
set -o allexport
source ./config.env
set +o allexport

python observer.py $IMAGES_DIR &
python server.py
