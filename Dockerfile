# Use an official Python runtime as a parent image
FROM jitesoft/tesseract-ocr
RUN train-lang eng --best
RUN train-lang ukr --best

USER root
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y python3-pip python3-dev libopencv-dev 

# Set the working directory to /app
WORKDIR /app
COPY ./requirements.txt /app
# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Copy the current directory contents into the container at /app
COPY *.py /app/
#COPY ./config.env /app/
#COPY ./start.sh /app/


# Run the command to start the Tesseract OCR process
ENTRYPOINT ["python3","observer.py"]

